﻿using UnityEngine;

namespace com.FDT.Pools
{
    /// <summary>
    /// Product Name:    FDT Pools
    /// Developers:      FDT Dev
    /// Company:         FDT Dev
    /// Description:     Interface implemented by objects that can be used in IPool type objects
    /// Changelog:       
    /// </summary>
    public interface IPooleable
    {
        PoolData poolData {get;}
        
        Transform transform { get; }
        GameObject gameObject { get; }
        bool isActiveAndEnabled { get; }
    }
}